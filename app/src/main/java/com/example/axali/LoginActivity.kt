package com.example.axali

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_first.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()
    }

    private fun init() {
        logInButton.setOnClickListener {
            check()
        }
    }

    private fun check() {
        val a = "aleksandre.kopaladze.1@btu.edu.ge"
        val b = "btu1234"
        if (EmailText.text.isNotEmpty() && (PasswordText.text.isNotEmpty()) && a in EmailText.text && b in PasswordText.text) {
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        } else {
           Toast.makeText(this, "Please check Email adress (use ...@gmail.com) and fill all fields", Toast.LENGTH_LONG).show()
        }
    }




}
