package com.example.axali

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val linearLayout = findViewById<LinearLayout>(R.id.linearLayout)

        val imageView = ImageView(this)

        Glide.with(this)
            .load("https://images.unsplash.com/photo-1531804055935-76f44d7c3621?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80")
            .into(imageView)
        linearLayout.addView(imageView)
        val linearLayout1 = findViewById<LinearLayout>(R.id.linearLayout1)
        val imageView1 = ImageView(this)
        Glide.with(this)
            .load("https://s3.amazonaws.com/appsdeveloperblog/Micky.jpg")
            .into(imageView1)
        linearLayout1.addView(imageView1)


    }

}
